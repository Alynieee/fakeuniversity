Rails.application.routes.draw do
  get 'welcome/index'

  resources :classes
  
  root 'welcome#index'
end
